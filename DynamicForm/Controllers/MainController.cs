﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DynamicForm.Models;

namespace DynamicForm.Controllers
{
    public class MainController : Controller
    {
        public ActionResult Index()
        {
            var initialData = new[]
            {
                new Gift {Name = "Tall Hat", Price = 39.95},
                new Gift {Name = "Long Cloak", Price = 120.00},
                new Gift {Name = "New gift", Price = 2.50},
            };
            return View(initialData);
        }

        [HttpPost]
        public ActionResult Index(IEnumerable<Gift> gifts)
        {
            return View("Gifts", gifts);
        }

        public ActionResult BlankEditorRow()
        {
            return View("GiftEditorRow", new Gift());
        }
    }
}